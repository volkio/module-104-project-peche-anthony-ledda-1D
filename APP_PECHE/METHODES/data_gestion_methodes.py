# data_gestion_methodes.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_methodes
from flask import flash

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionMethodes:
    def __init__(self):
        try:
            # Affichage des données dans la console
            print("dans le try de gestions methodes")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            print(f"Exception grave Classe constructeur GestionMethodes {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionMethodes ")

    def methodes_afficher_data(self):
        try:
            strsql_methodes_afficher = """SELECT id_Methode, NomMethode FROM t_methodes WHERE ValideMethode=1 """
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_methodes_afficher)
                # Récupère les données de la requête.
                data_methodes = mc_afficher.fetchall()
                # Retourne les données du "SELECT"
                return data_methodes
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def methodes_afficher_byid_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_methodes"
            strsql_methodes_afficher = """SELECT * FROM t_methodes ORDER BY id_Methode ASC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_methodes_afficher)
                # Récupère les données de la requête.
                data_methodes = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_methodes ", data_methodes, " Type : ", type(data_methodes))
                # Retourne les données du "SELECT"
                return data_methodes
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_methode_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # Anthony L 2020.04.07 Insertion t_methodes MySql
            strsql_insert_methode = """INSERT INTO t_methodes (id_Methode, NomMethode, ValideMethode) 
                                      VALUES (NULL,%(value_NomMethode)s,1)"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_methode, valeurs_insertion_dictionnaire)

        except pymysql.err.IntegrityError as erreur:
            # Anthony L 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")
        
    def edit_methode_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Anthony L 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le methode sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_methode = "SELECT * FROM t_methodes WHERE id_Methode = %(value_id_Methode)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_methode, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Anthony L 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_methode_data Data Gestions methodes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions methodes numéro de l'erreur : {erreur}", "danger")
            # Anthony L 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_methode_data d'un methode Data Gestions methodes {erreur}")

    def update_methode_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Anthony L 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntitulemethodeHTML" du form HTML "methodesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntitulemethodeHTML" value="{{ row.NomPers }}"/></td>
            str_sql_update_NomPers = """UPDATE t_methodes SET 
                                        NomMethode = %(value_NomMethode)s,
                                        ValideMethode = %(value_ValideMethode)s
                                        WHERE id_Methode = %(value_id_Methode)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_NomPers, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Anthony L 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_methode_data Data Gestions methodes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions methodes numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_methode_data d\'un methode Data Gestions methodes {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon !!! Introduire une valeur différente')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_methode_data Data Gestions methodes numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_methode_data d'un methode DataGestionsmethodes {erreur}")

    def delete_select_methode_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Anthony L 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntitulemethodeHTML" du form HTML "methodesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntitulemethodeHTML" value="{{ row.NomPers }}"/></td>

            # Anthony L 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le methode sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_methode = """SELECT * FROM t_methodes WHERE id_Methode = %(value_id_Methode)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_methode, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_select_methode_data Gestions methodes numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_methode_data numéro de l'erreur : {erreur}", "danger")
            raise Exception("Raise exception... Problème delete_select_methode_data d\'un methode Data Gestions methodes {erreur}")


    def delete_methode_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Anthony L 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "methodesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntitulemethodeHTML" value="{{ row.NomPers }}"/></td>
            str_sql_delete_NomPers = "DELETE FROM t_methodes WHERE id_Methode = %(value_id_Methode)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_NomPers, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_methode_data Data Gestions methodes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions methodes numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Anthony L 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un methode qui est associé à une methode dans la table intermédiaire "t_methodes_methode"
                # il y a une contrainte sur les FK de la table intermédiaire "t_methodes_methodes"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce methode est associé à des methodes dans la t_methodes_films !!! : {erreur}", "danger")
                # Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce methode est associé à des methodees dans la t_pers_a_methodes !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")