// modal.js

// Edit modal

// Get the modal
var modal = document.getElementById("myModal");
// Get the button that opens the modal
var btn = document.getElementById("editBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// Delete modal

// Get the modal
var deleteModal = document.getElementById("deleteModal");
// Get the button that opens the modal
var deleteBtn = document.getElementById("deleteBtn");
// Get the <span> element that closes the modal
var deleteClose = document.getElementById("deleteClose");
var deleteCancel = document.getElementById("deleteCancel");

// When the user clicks the button, open the modal
deleteBtn.onclick = function() {
  modal.style.display = "none";
  deleteModal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
deleteClose.onclick = function() {
  deleteModal.style.display = "none";
}

// When the user clicks on cancel button, close the modal
deleteCancel.onclick = function() {
  deleteModal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == deleteModal) {
    deleteModal.style.display = "none";
  }
}