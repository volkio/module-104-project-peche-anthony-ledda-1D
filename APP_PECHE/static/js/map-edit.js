//map.js

mapboxgl.accessToken = 'pk.eyJ1Ijoidm9sa2lvIiwiYSI6ImNrNzV1bDM5ajB6Y2kzbG52MGtjYTl5aW4ifQ.kOlnW2MuCgHexEL1pEV5Dg';
var lat_html = document.getElementById('lat').value
var lng_html = document.getElementById('lng').value
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/satellite-v9',
center: [lng_html, lat_html],
zoom: 10
});
map.setMaxZoom(12); // max zoom
// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());

var marker = new mapboxgl.Marker({
draggable: true
})
.setLngLat([lng_html, lat_html])
.addTo(map);

marker.on('dragend', onDragEnd);

// Function Draggable
function onDragEnd() {
    var lngLat = marker.getLngLat();
    document.getElementById('lat').value = lngLat.lat; //latitude
    document.getElementById('lng').value = lngLat.lng; //longitude
}