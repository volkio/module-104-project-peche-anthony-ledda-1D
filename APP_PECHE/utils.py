# utils.py
from _md5 import md5

from hashids import Hashids


# hash un id
def create_hashid(id):
    hashids = Hashids(min_length=10, salt='\xf2q\xbe8\xdf0 \x97\x0e\xdd`\xf6\xc4\xdc%2\x96@\x89\xf09\x982\x93')
    hashid = hashids.encode(id)
    return hashid

def decode_hashid(hashid):
    hashids = Hashids(min_length=10, salt='\xf2q\xbe8\xdf0 \x97\x0e\xdd`\xf6\xc4\xdc%2\x96@\x89\xf09\x982\x93')
    id = hashids.decode(hashid)
    return id

# hash l'id d'un user
def create_hashuserid(id):
    hashid = md5(id.encode(encoding='UTF-8')).hexdigest()
    return hashid