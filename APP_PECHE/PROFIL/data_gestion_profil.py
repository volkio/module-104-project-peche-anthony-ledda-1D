# data_gestion_profil.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_profil
from flask import flash, session

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionProfil:
    def __init__(self):
        try:
            # DEBUG bidon dans la console
            print("dans le try de gestions profil")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            # DEBUG bidon : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionProfil {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionProfil ")

    def profil_afficher_data(self):
        try:
            # Anthony L 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_mails"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_profil_afficher_data = """SELECT * FROM t_users"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
                valeur_id_current_profil_dictionnaire = {"value_id_current_profile": session['id_account']}
                strsql_profil_afficher_data += """ HAVING id_User= %(value_id_current_profile)s"""
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_profil_afficher_data, valeur_id_current_profil_dictionnaire)
                # Récupère les données de la requête.
                account = mc_afficher.fetchone()
                # Retourne les données du "SELECT"
                return account

        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # Anthony L 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def profil_update_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)

            # Anthony L 2019.04.02 Commande MySql pour la MODIFICATION de la valeur
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_profil = """UPDATE `t_users` SET `NomPers` = %(value_NomPers)s, 
                                        `PrenomPers` = %(value_PrenomPers)s, `DateNaissPers` = %(value_DateNaissPers)s,
                                        `User` = %(value_User)s, `Avatar` = %(value_Avatar)s,
                                        `Mail` = %(value_Mail)s
                                        WHERE id_User= %(value_id_current_profile)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_profil, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Anthony L 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_profil_data Data Gestions profils numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions profils numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_profil_data d\'un profil Data Gestions profils {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon !!! Introduire une valeur différente')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_profil_data Data Gestions profils numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_profil_data d'un profil DataGestionsprofils {erreur}")

    def profile_delete_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # AL 2020.05.12 Commande MySql pour EFFACER les valeures sélectionnées
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_id_User_Prise = "DELETE FROM t_user_e_prise WHERE fk_User = %(value_id_User)s"
            str_sql_delete_id_User_Equip = "DELETE FROM t_user_a_equip WHERE fk_User = %(value_id_User)s"
            str_sql_delete_id_User = "DELETE FROM t_users WHERE id_User = %(value_id_User)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_id_User_Prise, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    mc_cur.execute(str_sql_delete_id_User_Equip, valeur_delete_dictionnaire)
                    data_one += mc_cur.fetchall()
                    mc_cur.execute(str_sql_delete_id_User, valeur_delete_dictionnaire)
                    data_one += mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # AL 2020.05.12 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un prise qui est associé à une prise dans la table intermédiaire
                # il y a une contrainte sur les FK de la table intermédiaire
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce prise est associé à des prises dans la t_prises_films !!! : {erreur}", "danger")
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !!! Ce prise est associé à des prisees dans la t_pers_e_prise !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")