# routes_gestion_prises.py
# AL 2020.05.06 prises des "routes" FLASK pour les prises.
from _md5 import md5

from flask import render_template, flash, redirect, url_for, request, session
from APP_PECHE import app
from APP_PECHE.ESPECES.data_gestion_especes import GestionEspeces
from APP_PECHE.METHODES.data_gestion_methodes import GestionMethodes
from APP_PECHE.PRISES.data_gestion_prises import GestionPrises
from APP_PECHE.DATABASE.erreurs import *
import os
from werkzeug.utils import secure_filename
# AL 2020.05.10 Pour utiliser les expressions régulières REGEX
import re

# ---------------------------------------------------------------------------------------------------
# AL 2020.05.07 Définition d'une "route" /prises/cards
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/prises_afficher
# ---------------------------------------------------------------------------------------------------
from APP_PECHE.utils import decode_hashid


@app.route("/prises/cards/<string:order_by>", methods=['GET', 'POST'])
def prises_card_afficher(order_by):
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_prises = GestionPrises()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionprises()
            # Fichier data_gestion_prises.py
            data_prises = obj_actions_prises.prises_card_afficher_data()
            data_prises_ASC = obj_actions_prises.prises_card_afficher_data_ASC()

            if order_by == "ASC":
                print(" data prises", data_prises_ASC, "type ", type(data_prises_ASC))
                # AL 2020.05.12 Envoie la page "HTML" au serveur.
                return render_template("prises/prises_cards.html",
                                       data=data_prises_ASC,
                                       order_by=order_by)

            else:
                print(" data prises", data_prises, "type ", type(data_prises))
                # AL 2020.05.12 Envoie la page "HTML" au serveur.
                return render_template("prises/prises_cards.html",
                                       data=data_prises,
                                       order_by=order_by)

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")


@app.route("/prises/cards/personal", methods=['GET', 'POST'])
def prises_card_personal_afficher():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            if 'loggedin' in session:
                # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_prises = GestionPrises()
                # Récupére les données grâce à une requête MySql définie dans la classe Gestionprises()
                # Fichier data_gestion_prises.py
                data_prises = obj_actions_prises.prises_card_afficher_personal_data()

                # AL 2020.05.12 Envoie la page "HTML" au serveur.
                return render_template("prises/prises_cards.html",
                                       data=data_prises)
            return redirect(url_for("login"))
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.07 Définition d'une "route" /prises_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/prises_afficher
# ---------------------------------------------------------------------------------------------------
@app.route("/prises/afficher/<hashid>", methods=['GET', 'POST'])
def prises_afficher(hashid):
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            id_prise_sel = decode_hashid(hashid)
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_prises = GestionPrises()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionprises()
            # Fichier data_gestion_prises.py
            data_prises = obj_actions_prises.prises_afficher_data(id_prise_sel)
            data_prises_info = obj_actions_prises.prises_afficher_info_data(id_prise_sel)

            # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données prises affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # AL 2020.05.12 Envoie la page "HTML" au serveur.
    return render_template("prises/prises_afficher.html",
                           data=data_prises,
                           data_prises_info=data_prises_info)


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.07 Définition d'une "route" /prises/afficher/personal
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/prises_afficher
# ---------------------------------------------------------------------------------------------------
@app.route("/prises/afficher/personal/<hashid>", methods=['GET', 'POST'])
def prises_afficher_personal(hashid):
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            id_prise_sel = decode_hashid(hashid)
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_prises = GestionPrises()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionprises()
            # Fichier data_gestion_prises.py
            data_prises = obj_actions_prises.prises_afficher_data(id_prise_sel)
            data_prises_info = obj_actions_prises.prises_afficher_info_data(id_prise_sel)

            # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données prises affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # AL 2020.05.12 Envoie la page "HTML" au serveur.
    return render_template("prises/prises_afficher_personal.html",
                           data=data_prises,
                           data_prises_info=data_prises_info)


def allowed_image(filename):
    # On accepte seulement les fichier avec un "."
    if not "." in filename:
        return False

    # Séparer le nom de l'extension
    ext = filename.rsplit(".", 1)[1]

    # Verifier que le fichier a une extension qui correspond à ALLOWED_IMAGE_EXTENSIONS
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False

# ---------------------------------------------------------------------------------------------------
# AL 2020.05.12 Définition d'une "route" /prises_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "prises_add.html"
# Pour la tester http://127.0.0.1:1234/prises_add
# ---------------------------------------------------------------------------------------------------
@app.route("/prises/add", methods=['GET', 'POST'])
def prises_add():
    obj_actions_prises = GestionPrises()
    obj_actions_methodes = GestionMethodes()
    data_methodes = obj_actions_methodes.methodes_afficher_data()
    obj_actions_especes = GestionEspeces()
    data_especes = obj_actions_especes.especes_afficher_data()
    if 'loggedin' in session:

        # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
        # ou un envoi de donnée par des champs utilisateurs.
        if request.method == "POST":
            try:
                # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_prises = GestionPrises()
                # AL 2020.05.12 Récupère le contenu du champ dans le formulaire HTML "prise_add.html"
                NomMethode = request.form['Methode_select']
                NomEspece = request.form['Espece_select']
                PoidsPrise = request.form['PoidsPrise_html']
                TaillePrise = request.form['TaillePrise_html']
                DatePrise = request.form['DatePrise_html']
                HeurePrise = request.form['HeurePrise_html']
                GpsLatLocalisation = request.form['lat_html']
                GpsLonLocalisation = request.form['lng_html']
                PressionMeteo = request.form['pression_html']
                TemperatureMeteo = request.form['temp_html']
                ConditionMeteo = request.form['conditionMeteo_html']
                VentMeteo = request.form['vent_html']
                HumiditeMeteo = request.form['humidity_html']
                if 'file' not in request.files:
                    flash('No file part')
                    return redirect(request.url)
                file = request.files["file"]

                # vérification de la présence d'un nom à l'image
                if file.filename == "":
                    flash("No filename")
                    return redirect(request.url)
                # AL 2019.05.12 On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                # des valeurs avec des caractères qui ne sont pas des lettres. Accepte le trait d'union ou
                # l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.

                if not re.match("^\d+(\.\d{0,2})?$",
                                PoidsPrise):
                    # AL 2020.05.06 Message indiquant le probleme à l'utilisateur.
                    flash(f"Le poids n'est pas valide !!", "Danger")
                    # On doit afficher à nouveau le formulaire "prises_add.html" à cause des erreurs de "claviotage"
                    return redirect(url_for('prises_add'))

                if not re.match("^\d+$",
                                TaillePrise):
                    # AL 2020.05.06 Message indiquant le probleme à l'utilisateur.
                    flash(f"La taille n'est pas valide !!", "Danger")
                    # On doit afficher à nouveau le formulaire "prises_add.html" à cause des erreurs de "claviotage"
                    return redirect(url_for('prises_add'))

                else:
                    # vérification de la validité de l'image
                    if file and allowed_image(file.filename):
                        name = secure_filename(file.filename)
                        filename = md5(name.rsplit(".", 1)[0].encode(encoding='UTF-8')).hexdigest() + "." + name.rsplit(".", 1)[1]
                        # sauvegarde de l'image
                        file.save(os.path.join(app.config["IMAGE_UPLOADS"], filename))

                    # Constitution d'un dictionnaire et insertion dans la BD
                    valeurs_insertion_dictionnaire = {"value_PoidsPrise": PoidsPrise,
                                                      "value_TaillePrise": TaillePrise,
                                                      "value_DatePrise": DatePrise,
                                                      "value_HeurePrise": HeurePrise,
                                                      "value_PrisePhoto": filename,
                                                      "value_NomMethode": NomMethode,
                                                      "value_NomEspece": NomEspece,
                                                      "value_GpsLatLocalisation": GpsLatLocalisation,
                                                      "value_GpsLonLocalisation": GpsLonLocalisation,
                                                      "value_PressionMeteo": PressionMeteo,
                                                      "value_TemperatureMeteo": TemperatureMeteo,
                                                      "value_ConditionMeteo": ConditionMeteo,
                                                      "value_VentMeteo": VentMeteo,
                                                      "value_HumiditeMeteo": HumiditeMeteo}

                    obj_actions_prises.add_prise_data(valeurs_insertion_dictionnaire)
                    return redirect(url_for('profile'))

            # AL 2020.05.12 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
            except (pymysql.err.OperationalError,
                    pymysql.ProgrammingError,
                    pymysql.InternalError,
                    TypeError) as erreur:
                flash(f"Autre erreur {erreur}")
                raise MonErreur(f"Autre erreur")

            # AL 2020.05.12 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
            except Exception as erreur:
                # AL 2020.05.12 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
                # Ainsi on peut avoir un message d'erreur personnalisé.
                raise MaBdErreurConnexion(
                    f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
                # AL 2020.05.12 Envoie la page "HTML" au serveur.
        return render_template("prises/prises_add.html",
                               data_methodes=data_methodes,
                               data_especes=data_especes)
    return redirect(url_for("login"))


# AL 2020.05.12
# Définition d'une "route" /prises_edit , cela va permettre de programmer quelles actions sont réalisées avant de
# l'envoyer au navigateur par la méthode "render_template". On change la valeur d'un prise de prises par la commande
# MySql "UPDATE" ---------------------------------------------------------------------------------------------------
@app.route('/prises/edit/<hashuserid>/<hashid>', methods=['POST', 'GET'])
def prises_edit(hashuserid, hashid):
    # AL 2020.05.12 Les données sont affichées dans un formulaire
    obj_actions_prises = GestionPrises()
    obj_actions_methodes = GestionMethodes()
    data_methodes = obj_actions_methodes.methodes_afficher_data()
    obj_actions_especes = GestionEspeces()
    data_especes = obj_actions_especes.especes_afficher_data()

    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_Prise" du formulaire html "prises_card_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_Prise"
            # grâce à la variable "id_Prise_edit_html"
            # <a href="{{ url_for('prises_edit', id_Prise_edit_html=row.id_Prise) }}">Edit</a>
            id_Prise_edit = decode_hashid(hashid)
            id_User = decode_hashid(hashuserid)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_Prise": id_Prise_edit, "value_id_User": id_User}

            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_prises = GestionPrises()

            # AL 2020.05.12 La commande MySql est envoyée à la BD
            data_id_Prise = obj_actions_prises.edit_prise_data(valeur_select_dictionnaire)
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer une prise")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # AL 2020.05.12 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("prises/prises_edit.html",
                           data=data_id_Prise,
                           data_methodes=data_methodes,
                           data_especes=data_especes)


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.12 Définition d'une "route" /prises_update , cela va permettre de programmer quelles actions sont réalisées
# avant de l'envoyer au navigateur par la méthode "render_template".
# On change la valeur d'un prise de prises par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/prises/update', methods=['POST', 'GET'])
def prises_update():
    # AL 2020.05.12 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "prises_cards.html"
    # Une fois que l'utilisateur à modifié la valeur du prise alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"

    if request.method == 'POST':
        try:
            obj_actions_prises = GestionPrises()
            obj_actions_methodes = GestionMethodes()
            data_methodes = obj_actions_methodes.methodes_afficher_data()
            obj_actions_especes = GestionEspeces()
            data_especes = obj_actions_especes.especes_afficher_data()
            # Récupérer la valeur de "id_Prise" du formulaire html "prises_edit.html"

            id_Prise_edit = request.values['edit_id_Prise_html']

            # Récupère le contenu des champs dans le formulaire HTML "prises_edit.html"
            PrisePhoto = request.form['PrisePhoto_html']
            NomMethode = request.form['edit_Methode_select']
            NomEspece = request.form['edit_Espece_select']
            PoidsPrise = request.values['edit_PoidsPrise_html']
            TaillePrise = request.values['edit_TaillePrise_html']
            DatePrise = request.values['edit_DatePrise_html']
            HeurePrise = request.values['edit_HeurePrise_html']
            # Récupère contenu Geolocalisation
            GpsLatLocalisation = request.values['lat_html']
            GpsLonLocalisation = request.values['lng_html']
            # Récupère contenu Weather
            TemperatureMeteo = request.values['temp_html']
            PressionMeteo = request.values['pression_html']
            ConditionMeteo = request.values['conditionMeteo_html']
            HumiditeMeteo = request.values['humidity_html']
            VentMeteo = request.values['vent_html']

            valeur_edit_list = [{'id_Prise': id_Prise_edit, 'PrisePhoto': PrisePhoto, 'fk_Methode': NomMethode,
                                 'fk_Espece': NomEspece, 'PoidsPrise': PoidsPrise,
                                 'TaillePrise': TaillePrise, 'DatePrise': DatePrise,
                                 'HeurePrise': HeurePrise, 'GpsLatLocalisation': GpsLatLocalisation,
                                 'GpsLonLocalisation': GpsLonLocalisation, 'TemperatureMeteo': TemperatureMeteo,
                                 'PressionMeteo': PressionMeteo, 'ConditionMeteo': ConditionMeteo,
                                 'HumiditeMeteo': HumiditeMeteo, 'VentMeteo': VentMeteo, }]

            # Regex Poids Prise
            if not re.match("^\d+(\.\d{0,2})?$",
                            PoidsPrise):
                # AL 2020.05.06 Message indiquant le probleme à l'utilisateur.
                flash(f"Le poids n'est pas valide !!", "Danger")
                # On doit afficher à nouveau le formulaire "prises_add.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_Prise': id_Prise_edit, 'PrisePhoto': PrisePhoto, 'fk_Methode': NomMethode,
                                     'fk_Espece': NomEspece, 'PoidsPrise': PoidsPrise,
                                     'TaillePrise': TaillePrise, 'DatePrise': DatePrise,
                                     'HeurePrise': HeurePrise, 'GpsLatLocalisation': GpsLatLocalisation,
                                     'GpsLonLocalisation': GpsLonLocalisation, 'TemperatureMeteo': TemperatureMeteo,
                                     'PressionMeteo': PressionMeteo, 'ConditionMeteo': ConditionMeteo,
                                     'HumiditeMeteo': HumiditeMeteo, 'VentMeteo': VentMeteo, }]

                return render_template("prises/prises_edit.html",
                                       data=valeur_edit_list,
                                       data_methodes=data_methodes,
                                       data_especes=data_especes)

            # Regex Taille Prise
            if not re.match("^\d+$",
                            TaillePrise):
                # AL 2020.05.06 Message indiquant le probleme à l'utilisateur.
                flash(f"La taille n'est pas valide !!", "Danger")
                # On doit afficher à nouveau le formulaire "prises_add.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_Prise': id_Prise_edit, 'PrisePhoto': PrisePhoto, 'fk_Methode': NomMethode,
                                     'fk_Espece': NomEspece, 'PoidsPrise': PoidsPrise,
                                     'TaillePrise': TaillePrise, 'DatePrise': DatePrise,
                                     'HeurePrise': HeurePrise, 'GpsLatLocalisation': GpsLatLocalisation,
                                     'GpsLonLocalisation': GpsLonLocalisation, 'TemperatureMeteo': TemperatureMeteo,
                                     'PressionMeteo': PressionMeteo, 'ConditionMeteo': ConditionMeteo,
                                     'HumiditeMeteo': HumiditeMeteo, 'VentMeteo': VentMeteo, }]

                return render_template("prises/prises_edit.html",
                                       data=valeur_edit_list,
                                       data_methodes=data_methodes,
                                       data_especes=data_especes)

            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_Prise": id_Prise_edit,
                                              "value_fk_Methode": NomMethode,
                                              "value_fk_Espece": NomEspece,
                                              "value_PoidsPrise": PoidsPrise,
                                              "value_TaillePrise": TaillePrise,
                                              "value_DatePrise": DatePrise,
                                              "value_HeurePrise": HeurePrise,
                                              "value_GpsLatLocalisation": GpsLatLocalisation,
                                              "value_GpsLonLocalisation": GpsLonLocalisation,
                                              "value_TemperatureMeteo": TemperatureMeteo,
                                              "value_PressionMeteo": PressionMeteo,
                                              "value_ConditionMeteo": ConditionMeteo,
                                              "value_HumiditeMeteo": HumiditeMeteo,
                                              "value_VentMeteo": VentMeteo}

                # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_prises = GestionPrises()
                # La commande MySql est envoyée à la BD
                data_id_Prise = obj_actions_prises.update_prise_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataId_prise ", data_id_Prise, "type ", type(data_id_Prise))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer une prise")
                # On affiche les prises
                return redirect(url_for('profile'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème prises update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_intitule_prise_html" alors on renvoie le formulaire "EDIT"
            return render_template('prises/prises_edit.html',
                                   data=valeur_edit_list,
                                   data_methodes=data_methodes,
                                   data_especes=data_especes)


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.12 Définition d'une "route" /prises_select_delete , cela va permettre de programmer quelles actions
# sont réalisées avant de l'envoyer au navigateur par la méthode "render_template".
# On change la valeur d'un prise de prises par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/prises/select_delete/<hashuserid>/<hashid>', methods=['POST', 'GET'])
def prises_select_delete(hashuserid, hashid):
    if request.method == 'GET':
        try:

            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_prises = GestionPrises()
            # AL 2020.05.12 Récupérer la valeur de "idpriseDeleteHTML" du formulaire html "prisesDelete.html"
            id_Prise_delete = decode_hashid(hashid)
            id_User = decode_hashid(hashuserid)
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_Prise": id_Prise_delete, "value_id_User": id_User}

            # AL 2020.05.12 La commande MySql est envoyée à la BD
            data_id_Prise = obj_actions_prises.delete_select_prise_data(valeur_delete_dictionnaire)

            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # Pour afficher un message dans la console.
            print(f"Erreur prises_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur prises_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('prises/prises_delete.html',
                           data=data_id_Prise)


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.12 Définition d'une "route" /prisesUpdate , cela va permettre de programmer quelles actions sont réalisées
# avant de l'envoyer au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un prise, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/prises/delete', methods=['POST', 'GET'])
def prises_delete():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs
    # utilisateurs.
    if request.method == 'POST':
        try:
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_prises = GestionPrises()
            # AL 2020.05.12 Récupérer la valeur de "id_Prise"
            id_Prise_delete = request.form['id_Prise_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_Prise": id_Prise_delete}

            data_prises = obj_actions_prises.delete_prise_data(valeur_delete_dictionnaire)
            # AL 2020.05.12 On va afficher la liste des prises des prises
            # AL 2020.05.12 Envoie la page "HTML" au serveur.

            # On affiche les prises
            return redirect(url_for('profile'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # AL 2020.05.12 Traiter spécifiquement l'erreur MySql 1451 Cette erreur 1451, signifie qu'on veut effacer
            # un "prise" de prises qui est associé dans "t_pers_a_prises".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des prisess !')
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !! Ce prise est associé à des prisess dans la t_pers_a_prises !!! : {erreur}")
                # Afficher la liste des prises des prises
                return redirect(url_for('profile'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # Pour afficher un message dans la console.
                print(f"Erreur prises_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur prises_delete {erreur.args[0], erreur.args[1]}")
                # AL 2020.05.12 Envoie la page "HTML" au serveur.
                return render_template('profil/profil_afficher.html')
