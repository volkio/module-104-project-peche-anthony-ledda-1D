-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 02 Juillet 2020 à 20:07
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ledda_anthony_peche_info1d_2020`
--

-- --------------------------------------------------------

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists ledda_anthony_peche_info1d_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ledda_anthony_peche_info1d_2020;

-- Utilisation de cette base de donnée

USE ledda_anthony_peche_info1d_2020;
-- --------------------------------------------------------

--
-- Structure de la table `t_equipement`
--

CREATE TABLE `t_equipement` (
  `id_Equipement` int(11) NOT NULL,
  `NomEquipement` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_equip_a_mat`
--

CREATE TABLE `t_equip_a_mat` (
  `id_EquipAMat` int(11) NOT NULL,
  `fk_Equipement` int(11) NOT NULL,
  `fk_Materiel` int(11) NOT NULL,
  `DateMateriel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_especes`
--

CREATE TABLE `t_especes` (
  `id_Espece` int(11) NOT NULL,
  `ThumbnailEspeceLink` text,
  `NomEspece` varchar(30) NOT NULL,
  `NomScientifiqueEspece` varchar(30) DEFAULT NULL,
  `DescriptionEspece` text,
  `PoidsMaxEspece` decimal(10,1) DEFAULT NULL,
  `LongMaxEspece` int(5) DEFAULT NULL,
  `AgeMaxEspece` int(5) DEFAULT NULL,
  `ProfEspece` varchar(10) DEFAULT NULL,
  `PlanEauEspece` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_especes`
--

INSERT INTO `t_especes` (`id_Espece`, `ThumbnailEspeceLink`, `NomEspece`, `NomScientifiqueEspece`, `DescriptionEspece`, `PoidsMaxEspece`, `LongMaxEspece`, `AgeMaxEspece`, `ProfEspece`, `PlanEauEspece`) VALUES
(1, 'Perche.jpg', 'Perche', 'Perca fluviatilis', 'La perche commune est un poisson d\'eau douce de la famille des percidés. On la reconnaît à son flanc avec\r\n5-8 barres sombres, généralement en forme de Y, ses deux nageoires dorsales, clairement séparées l\'une de l\'autre. La première nageoire dorsale grise avec une tache noire à l\'extrémité, deuxième dorsale jaune verdâtre autres nageoires rouges. Un corps jaune verdâtre.  ', '4.8', 60, 22, '1 - 30 m', 'eau douce'),
(2, 'Brochet.jpg', 'Brochet', 'Esox lucius', 'Le brochet est un poisson prédateur commun en eaux douces et saumâtres dans l\'hémisphère nord. Les plus grands spécimens sont des femelles.Se distingue par son museau long et plat à bec de canard; sa grande bouche avec de nombreuses grandes dents pointues; et la position arrière de ses nageoires dorsale et anale. Dorsale située loin à l\'arrière, anale située sous et survenant un peu derrière la dorsale; ligne latérale crantée en arrière.', '28.0', 150, 30, '0 - 30 m', 'eau douce / saumatre'),
(3, 'Truite_Brune.jpg', 'Truite de lac (fario)', 'Salmo trutta', 'Salmo trutta est une espèce de poissons de la famille des Salmonidés qui correspond à la truite commune européenne. Son corps est de couleur gris-bleu avec de nombreuses taches, également en dessous de la ligne latérale. Couleur noirâtre sur la partie supérieure du corps, généralement orange sur les côtés, entourée de halos pâles. Nageoire adipeuse à bord rouge.', '50.0', 140, 38, '0 - 28 m', 'eau douce / salée'),
(4, 'Omble_Chevalier.jpg', 'Omble chevalier', 'Salvelinus alpinus', '', '15.0', 107, 40, '0 - 70 m', 'eau douce'),
(5, 'Chevaine.jpg', 'Chevaine', 'Squalius cephalus', '', '8.0', 60, 22, '0 - ? m', 'eau douce'),
(6, 'Truite_arc-en-ciel.jpg', 'Truite arc-en-ciel', 'Salmo gairdneri', 'La truite arc-en-ciel a une coloration qui varie selon l\'habitat, la taille et l\'état sexuel. Les résidents et les reproducteurs des cours d\'eau sont plus foncés, les couleurs sont plus intenses.\r\nLes résidents du lac sont plus clairs, plus brillants et plus argentés.\r\nLes mâles reproducteurs dépourvus de bosse; les jeunes sans marques de tacons; Elle possède une large bande rose à rouge de la tête à la base caudale, sauf sous forme de parcours marin', '25.4', 122, 11, '0 - 200 m', 'eau douce / salée'),
(7, 'Lotte.jpg', 'Lotte', 'Lota lota', '', '34.0', 152, 20, '1 - 700 m', 'eau douce / saumâtre'),
(8, 'Silure.jpg', 'Silure', 'Silurus glanis', 'Le silure est le plus gros poisson d\'eau douce d\'Europe. Il possède une épine dorsale, 4-5 Rayons mous dorsaux, une épine anale. \r\nIl se distinguent de tous les autres poissons d\'eau douce d\'Europe par ses deux paires de barbillons mentaux et sa nageoire anale avec 83-91½ rayons, son corps nu, sa grosse tête déprimée, sa nageoire caudale arrondie ou tronquée et les rayons anaux touchant presque la caudale.', '306.0', 500, 80, '0 - 30 m', 'eau douce'),
(9, 'Sandre.jpg', 'Sandre', 'Sander lucioperca', 'Le sandre possède 13 - 20 Épines dorsales, 2-3 Épines anales, 45 - 47 vertèbres. Il se distingue de ses congénères en Europe par ses 1-2 dents canines élargies dans la partie antérieure de chaque mâchoire, sa deuxième nageoire dorsale avec 18-22 rayons ramifiés et 80-97 écailles sur la ligne latérale.', '15.0', 125, 17, '2 - 30 m', 'eau douce / saumâtre'),
(10, 'Barbeau.jpg', 'Barbeau commun', 'Barbus barbus', '', '12.0', 120, 15, '10 m', 'eau douce'),
(11, 'Palee.jpg', 'Palée', 'Coregonus lavaretus', 'Le corégone blanc est un poisson d\'eau douce qui vit exclusivement dans les lacs alpins. Les corégones sont localement fréquents dans l\'hémisphère nord en Europe, en Asie et en Amérique du Nord où selon le lieu il est appelé : Marène, vendace, féra, lavaret, palée.\r\nIl possède une bouche petite avec une mâchoire supérieure saillante, un corps argenté et une nageoire caudale à 19 rayons. ', '3.0', 60, 10, '0 - 20 m', 'eau douce'),
(12, 'Anguille.jpg', 'Anguille', 'Anguilla anguilla', '', '6.6', 150, 88, ' 0 - 700 m', 'eau douce / salée'),
(13, 'Rotengle.jpg', 'Rotengle', 'Scardinius erythrophthalmus', 'Le rotengle est un poisson qui ressemble beaucoup au gardon. Il a un museau pointé vers l\'avant, pointe au niveau ou légèrement au-dessus du milieu de l\'œil; un dos non bossu derrière la nuque.\r\nToutes ses nageoires ont une teinte rougeâtre et la nageoire pelvienne rouge foncé. Il arrive fréquemment qu\'il s\'hybride au gardon car ils cohabitent souvent.', '2.1', 62, 19, '0 - ? m', 'eau douce'),
(14, 'Tanche.jpg', 'Tanche', 'Tinca tinca', 'La tanche est trapue, ovale et recouverte de petites écailles profondément incrustées dans une peau épaisse et visqueuse.\r\nElle possède deux barbillons aux coins des lèvres. Le dos a une couleur qui va de vert olive au vert brun avec des reflets doré sur la face ventrale.\r\nSa tête est triangulaire, petite, ses yeux sont rouges orangés.', '7.5', 70, 15, '1 - 3 m', 'eau douce'),
(15, 'Carpe.jpg', 'Carpe commune', 'Cyprinus carpio', '', '40.1', 120, 38, '0 - 29 m', 'eau douce / saumâtre'),
(16, 'Gardon.jpg', 'Gardon (Vengeron)', 'Rutilus rutilus', '', '1.8', 50, 14, '15 - ? m', 'eau douce'),
(17, 'Breme.jpg', 'Brème commune', 'Abramis brama', '', '6.0', 82, 23, '1 - ? m', 'eau douce'),
(18, 'Bondelle.jpg', 'Bondelle', 'Coregonus oxyrinchus', '', '2.0', 50, 10, '0 - 20 m', 'eau douce'),
(33, NULL, 'Autre', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_infos`
--

CREATE TABLE `t_infos` (
  `id_Info` int(11) NOT NULL,
  `ConditionMeteo` text,
  `TemperatureMeteo` int(5) DEFAULT NULL,
  `VentMeteo` decimal(5,2) DEFAULT NULL,
  `GpsLatLocalisation` double DEFAULT NULL,
  `GpsLonLocalisation` double DEFAULT NULL,
  `PressionMeteo` int(11) DEFAULT NULL,
  `HumiditeMeteo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_infos`
--

INSERT INTO `t_infos` (`id_Info`, `ConditionMeteo`, `TemperatureMeteo`, `VentMeteo`, `GpsLatLocalisation`, `GpsLonLocalisation`, `PressionMeteo`, `HumiditeMeteo`) VALUES
(1, 'peu nuageux', 20, '3.60', 46.819035801160226, 6.742153774484876, 1019, 64),
(2, 'peu nuageux', 20, '2.60', 46.82593138202816, 6.751002566957709, 1020, 56),
(3, 'peu nuageux', 24, '2.10', 46.535635873532755, 6.482084563524211, 1016, 51),
(4, 'peu nuageux', 22, '1.00', 46.81968783635057, 6.746812998761214, 1017, 56),
(5, 'légère pluie', 16, '3.60', 46.89323661810258, 6.815350903891584, 1015, 100),
(6, 'légère pluie', 22, '2.10', 48.829284860316136, 10.530415509481656, 1015, 69),
(7, 'nuageux', 19, '3.10', 46.46472932732581, 6.5318109550231895, 1018, 73),
(8, NULL, NULL, NULL, 46.670765063816106, 7.095768261048107, NULL, NULL),
(9, 'peu nuageux', 20, '2.60', 46.833120242335184, 6.7707954659467475, 1011, 49),
(10, 'légère pluie', 23, '2.10', 46.79357041611965, 6.662513388750483, 1019, 64),
(11, 'peu nuageux', 21, '1.00', 46.83188894019534, 6.75453149715662, 1016, 68),
(12, 'partiellement nuageux', 20, '3.10', 46.86169587853365, 6.818730062678867, 1017, 77),
(13, 'partiellement nuageux', 16, '1.50', 46.814735621550284, 6.759473923372013, 1016, 88),
(14, 'ciel dégagé', 19, '0.50', 46.80410954946893, 6.928434736448736, 1011, 60),
(15, 'peu nuageux', 22, '0.50', 46.81468492597256, 6.751005476482362, 1013, 64);

-- --------------------------------------------------------

--
-- Structure de la table `t_materiels`
--

CREATE TABLE `t_materiels` (
  `id_Materiel` int(11) NOT NULL,
  `fk_TypeMateriel` int(11) NOT NULL,
  `MarqueMateriel` varchar(25) NOT NULL,
  `ArticleMateriel` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_materiels`
--

INSERT INTO `t_materiels` (`id_Materiel`, `fk_TypeMateriel`, `MarqueMateriel`, `ArticleMateriel`) VALUES
(2, 4, '13 Fishing', 'Defy Silver'),
(3, 5, 'Abu Garcia', 'Abu Pro Max Spinning Reel'),
(4, 5, 'Dragon', 'Team Dragon Z Spinning Reel'),
(5, 1, 'Ace', 'Fat Flipper Small'),
(6, 5, 'Mitchell', 'Avocet RZ Reel'),
(7, 6, 'Berkley', 'Big Game');

-- --------------------------------------------------------

--
-- Structure de la table `t_methodes`
--

CREATE TABLE `t_methodes` (
  `id_Methode` int(11) NOT NULL,
  `NomMethode` varchar(30) NOT NULL,
  `ValideMethode` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_methodes`
--

INSERT INTO `t_methodes` (`id_Methode`, `NomMethode`, `ValideMethode`) VALUES
(1, 'Lancer', 1),
(2, 'Pêche au Jerkbait', 1),
(3, 'Pêche au coup', 1),
(4, 'Pêche au harpon', 1),
(5, 'Pêche de fond', 1),
(6, 'Pêche sur glace', 1),
(7, 'Pêche verticale', 1),
(8, 'Pêche à la ligne dans la vague', 1),
(9, 'Pêche à la ligne en mer', 1),
(10, 'Pêche à la ligne libre', 1),
(11, 'Pêche à la ligne a main', 1),
(12, 'Pêche à la mouche', 1),
(13, 'Pêche à la traine', 1),
(14, 'Pêche à la turlutte', 1),
(15, 'Pêche à la gambe', 1),
(16, 'Autre', NULL),
(17, 'Pêche à la nouille', 0);

-- --------------------------------------------------------

--
-- Structure de la table `t_photos`
--

CREATE TABLE `t_photos` (
  `id_Photo` int(11) NOT NULL,
  `PrisePhoto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_photos`
--

INSERT INTO `t_photos` (`id_Photo`, `PrisePhoto`) VALUES
(1, '7d554a1a0579becd90925c1c35b8cf04.jpg'),
(2, '632f57a30d2bf372cc04f22c1b9c0ae9.JPG'),
(3, '7e071323818e70bb729ecc455df155ba.jpg'),
(4, 'aacbb4525926d0902bbff28896b5901a.jpg'),
(5, '4d28d5ac72460132c11f030ce057efcb.jpg'),
(6, '3ed257cf8f22383b75140320d9858a43.jpg'),
(7, '7d554a1a0579becd90925c1c35b8cf04.jpg'),
(8, '7e071323818e70bb729ecc455df155ba.jpg'),
(9, '632f57a30d2bf372cc04f22c1b9c0ae9.JPG'),
(10, '66dedc20a1e64a6b5e4be69b7f88fb58.png'),
(11, '4d28d5ac72460132c11f030ce057efcb.jpg'),
(12, '7d554a1a0579becd90925c1c35b8cf04.jpg'),
(13, 'aacbb4525926d0902bbff28896b5901a.jpg'),
(14, 'd4828886b79ee94fe856595111c7600e.jpg'),
(15, '66dedc20a1e64a6b5e4be69b7f88fb58.png');

-- --------------------------------------------------------

--
-- Structure de la table `t_prises`
--

CREATE TABLE `t_prises` (
  `id_Prise` int(11) NOT NULL,
  `fk_Methode` int(11) NOT NULL,
  `fk_Espece` int(11) NOT NULL,
  `PoidsPrise` decimal(10,1) NOT NULL,
  `TaillePrise` int(11) NOT NULL,
  `DatePrise` date NOT NULL,
  `HeurePrise` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_prises`
--

INSERT INTO `t_prises` (`id_Prise`, `fk_Methode`, `fk_Espece`, `PoidsPrise`, `TaillePrise`, `DatePrise`, `HeurePrise`) VALUES
(1, 1, 2, '10.5', 113, '2020-06-23', '10:00:00'),
(2, 13, 2, '1.0', 80, '2020-06-24', '11:11:00'),
(3, 6, 18, '0.1', 4, '2020-06-25', '20:16:00'),
(4, 4, 8, '30.0', 120, '2020-06-25', '10:10:00'),
(5, 1, 6, '3.0', 100, '2020-06-29', '10:10:00'),
(6, 12, 6, '10.0', 69, '2020-06-29', '10:10:00'),
(7, 1, 2, '10.0', 113, '2020-06-28', '10:10:00'),
(8, 1, 18, '0.4', 45, '2018-05-16', '11:00:00'),
(9, 1, 2, '5.0', 89, '2020-06-30', '12:00:00'),
(10, 16, 33, '10.0', 100, '2020-06-26', '10:10:00'),
(11, 12, 6, '10.0', 100, '2020-06-27', '10:10:00'),
(12, 1, 2, '10.0', 113, '2020-06-28', '11:01:00'),
(13, 11, 8, '10.0', 200, '2020-06-29', '10:10:00'),
(14, 1, 2, '10.0', 113, '2020-06-30', '10:10:00'),
(15, 7, 33, '10.0', 10, '2020-07-01', '10:10:00');

-- --------------------------------------------------------

--
-- Structure de la table `t_prise_a_info`
--

CREATE TABLE `t_prise_a_info` (
  `id_PriseAInfo` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `fk_Info` int(11) NOT NULL,
  `DateInfo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_prise_a_info`
--

INSERT INTO `t_prise_a_info` (`id_PriseAInfo`, `fk_Prise`, `fk_Info`, `DateInfo`) VALUES
(1, 1, 1, '2020-06-25 16:44:33'),
(2, 2, 2, '2020-06-25 17:56:17'),
(3, 3, 3, '2020-06-27 13:11:59'),
(4, 4, 4, '2020-06-27 14:33:58'),
(5, 5, 5, '2020-06-30 06:26:19'),
(6, 6, 6, '2020-06-30 06:43:05'),
(7, 7, 7, '2020-06-30 07:56:23'),
(8, 8, 8, '2020-06-30 09:35:34'),
(9, 9, 9, '2020-06-30 10:27:25'),
(10, 10, 10, '2020-06-30 14:27:04'),
(11, 11, 11, '2020-06-30 21:06:58'),
(12, 12, 12, '2020-06-30 21:09:12'),
(13, 13, 13, '2020-06-30 21:32:01'),
(14, 14, 14, '2020-07-01 18:29:48'),
(15, 15, 15, '2020-07-01 23:30:22');

-- --------------------------------------------------------

--
-- Structure de la table `t_prise_a_photo`
--

CREATE TABLE `t_prise_a_photo` (
  `id_PriseAPhoto` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `fk_Photo` int(11) NOT NULL,
  `DatePhoto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_prise_a_photo`
--

INSERT INTO `t_prise_a_photo` (`id_PriseAPhoto`, `fk_Prise`, `fk_Photo`, `DatePhoto`) VALUES
(3, 3, 3, '2020-06-27 13:11:59'),
(8, 8, 8, '2020-06-30 09:35:34'),
(9, 9, 9, '2020-06-30 10:27:25'),
(12, 12, 12, '2020-06-30 21:09:12');

-- --------------------------------------------------------

--
-- Structure de la table `t_typemateriel`
--

CREATE TABLE `t_typemateriel` (
  `id_TypeMateriel` int(11) NOT NULL,
  `NomTypeMateriel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_typemateriel`
--

INSERT INTO `t_typemateriel` (`id_TypeMateriel`, `NomTypeMateriel`) VALUES
(1, 'Appats et leurres'),
(2, 'Natural baits'),
(3, 'Flies'),
(4, 'Cannes'),
(5, 'Moulinets'),
(6, 'Combos'),
(7, 'Lignes'),
(8, 'Accessoires'),
(9, 'Autres');

-- --------------------------------------------------------

--
-- Structure de la table `t_users`
--

CREATE TABLE `t_users` (
  `id_User` int(11) NOT NULL,
  `NomPers` text,
  `PrenomPers` text,
  `DateNaissPers` date DEFAULT NULL,
  `User` text,
  `Password` text,
  `Avatar` text CHARACTER SET utf8mb4,
  `Mail` text,
  `Rank` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_users`
--

INSERT INTO `t_users` (`id_User`, `NomPers`, `PrenomPers`, `DateNaissPers`, `User`, `Password`, `Avatar`, `Mail`, `Rank`) VALUES
(1, 'Ledda', 'Anthony', '2003-12-11', 'Volkio', '$2b$12$3aEO5yZNDHgzBNZqzqsS5.K2XBE.dhrZyT6sXoeP9mB5qeBC92QvS', 'https://www.gravatar.com/avatar/b842a2f9317126ca3e751b6711e7e695', 'volkioyt@gmail.com', 10),
(2, NULL, NULL, NULL, 'flotaro', '$2b$12$uoEg09nUG84Cx2KL/PTK8.HgONzb7X2.CY6.KV9o10h11xbof/YJC', NULL, 'flotaro@gmail.com', 1),
(3, 'rwe', 'erwr', '2020-06-02', 'ewr', '$2b$12$XCVc20tlB4A0nOIBdpis6OrK/tbZxNF08n0yoqjf/zkdsaehZPAkS', 'rewr', 'eeee@gmail.com', 1),
(5, NULL, NULL, NULL, 'Activytree', '$2b$12$brHmIbaDZg5MqU7YOvW5BeW.VxHS4JCHxxZYs5yc/5ModyT0I3fyK', 'https://www.gravatar.com/avatar/45e1e8b78a1ea41bfe8f4a226deb1b8f', 'dimitri.rutz@sunrise.ch', 1),
(6, NULL, NULL, NULL, 'Roccom14', '$2b$12$m/UGHFjWKcU.hPIM.3UIAOZSiE/6L8PyOMAFodnZRfArW/w.AfFm2', 'https://www.gravatar.com/avatar/4c49f17b25190cefcac4455ea0e243a2', 'rocco.ronzano@epfl.ch', 1),
(8, 'Ledda', 'Tommaso', '1973-08-25', 'Tommaso', '$2b$12$GqDmd/pvea.kOzVW27Swqe7IfE2cFJLktkXm1L.BYtS3NtLRSoW6q', 'https://www.gravatar.com/avatar/2a4ae1d2f935ab4595c8bc19fc9b387e', 'tommaso@ledda.ch', 1),
(9, NULL, NULL, NULL, 'vogonAmiral', '$2b$12$GfqXpROF13MkZ/h3tFqSnOUXTxrXjLKeVda0dqUHSs2IxkPmAiL36', 'https://www.gravatar.com/avatar/90fe2031c261d35a4a242f790fa34746', 'vogonamiral@epsic.ch', 3),
(10, NULL, NULL, NULL, 'admin', '$2b$12$L3lW6qX3qCEbR0hM4qMsMewskoERT0d2JFJl1LZfXZqlyLCiOiUa2', 'https://www.gravatar.com/avatar/557f00bb3768b1f9df5bcf75120f72fc', 'admin@admin.co', 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_user_a_equip`
--

CREATE TABLE `t_user_a_equip` (
  `id_UserAEquip` int(11) NOT NULL,
  `fk_User` int(11) NOT NULL,
  `fk_Equipement` int(11) NOT NULL,
  `DateEquip` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_user_e_prise`
--

CREATE TABLE `t_user_e_prise` (
  `id_UserEPrise` int(11) NOT NULL,
  `fk_User` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `DateUserPrise` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_user_e_prise`
--

INSERT INTO `t_user_e_prise` (`id_UserEPrise`, `fk_User`, `fk_Prise`, `DateUserPrise`) VALUES
(1, 1, 1, '2020-06-25 16:44:33'),
(2, 1, 2, '2020-06-25 17:56:17'),
(4, 1, 4, '2020-06-27 14:33:58'),
(5, 1, 5, '2020-06-30 06:26:19'),
(6, 1, 6, '2020-06-30 06:43:05'),
(7, 1, 7, '2020-06-30 07:56:23'),
(8, 1, 8, '2020-06-30 09:35:34'),
(9, 8, 9, '2020-06-30 10:27:25'),
(10, 1, 10, '2020-06-30 14:27:04'),
(11, 1, 11, '2020-06-30 21:06:58'),
(12, 1, 12, '2020-06-30 21:09:12'),
(13, 1, 13, '2020-06-30 21:32:01'),
(14, 1, 14, '2020-07-01 18:29:48'),
(15, 1, 15, '2020-07-01 23:30:22');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_equipement`
--
ALTER TABLE `t_equipement`
  ADD PRIMARY KEY (`id_Equipement`);

--
-- Index pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  ADD PRIMARY KEY (`id_EquipAMat`),
  ADD KEY `fk_Equipement` (`fk_Equipement`),
  ADD KEY `fk_Materiel` (`fk_Materiel`);

--
-- Index pour la table `t_especes`
--
ALTER TABLE `t_especes`
  ADD PRIMARY KEY (`id_Espece`);

--
-- Index pour la table `t_infos`
--
ALTER TABLE `t_infos`
  ADD PRIMARY KEY (`id_Info`);

--
-- Index pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  ADD PRIMARY KEY (`id_Materiel`),
  ADD KEY `fk_TypeMateriel` (`fk_TypeMateriel`);

--
-- Index pour la table `t_methodes`
--
ALTER TABLE `t_methodes`
  ADD PRIMARY KEY (`id_Methode`);

--
-- Index pour la table `t_photos`
--
ALTER TABLE `t_photos`
  ADD PRIMARY KEY (`id_Photo`);

--
-- Index pour la table `t_prises`
--
ALTER TABLE `t_prises`
  ADD PRIMARY KEY (`id_Prise`),
  ADD KEY `fk_Methode` (`fk_Methode`),
  ADD KEY `fk_Especes` (`fk_Espece`);

--
-- Index pour la table `t_prise_a_info`
--
ALTER TABLE `t_prise_a_info`
  ADD PRIMARY KEY (`id_PriseAInfo`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Info` (`fk_Info`);

--
-- Index pour la table `t_prise_a_photo`
--
ALTER TABLE `t_prise_a_photo`
  ADD PRIMARY KEY (`id_PriseAPhoto`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Photo` (`fk_Photo`);

--
-- Index pour la table `t_typemateriel`
--
ALTER TABLE `t_typemateriel`
  ADD PRIMARY KEY (`id_TypeMateriel`);

--
-- Index pour la table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id_User`);

--
-- Index pour la table `t_user_a_equip`
--
ALTER TABLE `t_user_a_equip`
  ADD PRIMARY KEY (`id_UserAEquip`),
  ADD KEY `fk_Personne` (`fk_User`),
  ADD KEY `fk_Equipement` (`fk_Equipement`);

--
-- Index pour la table `t_user_e_prise`
--
ALTER TABLE `t_user_e_prise`
  ADD PRIMARY KEY (`id_UserEPrise`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Personne` (`fk_User`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_equipement`
--
ALTER TABLE `t_equipement`
  MODIFY `id_Equipement` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  MODIFY `id_EquipAMat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_especes`
--
ALTER TABLE `t_especes`
  MODIFY `id_Espece` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `t_infos`
--
ALTER TABLE `t_infos`
  MODIFY `id_Info` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  MODIFY `id_Materiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_methodes`
--
ALTER TABLE `t_methodes`
  MODIFY `id_Methode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `t_photos`
--
ALTER TABLE `t_photos`
  MODIFY `id_Photo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_prises`
--
ALTER TABLE `t_prises`
  MODIFY `id_Prise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_prise_a_info`
--
ALTER TABLE `t_prise_a_info`
  MODIFY `id_PriseAInfo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_prise_a_photo`
--
ALTER TABLE `t_prise_a_photo`
  MODIFY `id_PriseAPhoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `t_typemateriel`
--
ALTER TABLE `t_typemateriel`
  MODIFY `id_TypeMateriel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id_User` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `t_user_a_equip`
--
ALTER TABLE `t_user_a_equip`
  MODIFY `id_UserAEquip` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_user_e_prise`
--
ALTER TABLE `t_user_e_prise`
  MODIFY `id_UserEPrise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  ADD CONSTRAINT `t_equip_a_mat_ibfk_1` FOREIGN KEY (`fk_Equipement`) REFERENCES `t_equipement` (`id_Equipement`),
  ADD CONSTRAINT `t_equip_a_mat_ibfk_2` FOREIGN KEY (`fk_Materiel`) REFERENCES `t_materiels` (`id_Materiel`);

--
-- Contraintes pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  ADD CONSTRAINT `t_materiels_ibfk_1` FOREIGN KEY (`fk_TypeMateriel`) REFERENCES `t_typemateriel` (`id_TypeMateriel`);

--
-- Contraintes pour la table `t_prises`
--
ALTER TABLE `t_prises`
  ADD CONSTRAINT `t_prises_ibfk_1` FOREIGN KEY (`fk_Methode`) REFERENCES `t_methodes` (`id_Methode`),
  ADD CONSTRAINT `t_prises_ibfk_2` FOREIGN KEY (`fk_Espece`) REFERENCES `t_especes` (`id_Espece`);

--
-- Contraintes pour la table `t_prise_a_info`
--
ALTER TABLE `t_prise_a_info`
  ADD CONSTRAINT `t_prise_a_info_ibfk_1` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`),
  ADD CONSTRAINT `t_prise_a_info_ibfk_2` FOREIGN KEY (`fk_Info`) REFERENCES `t_infos` (`id_Info`);

--
-- Contraintes pour la table `t_prise_a_photo`
--
ALTER TABLE `t_prise_a_photo`
  ADD CONSTRAINT `t_prise_a_photo_ibfk_1` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`),
  ADD CONSTRAINT `t_prise_a_photo_ibfk_2` FOREIGN KEY (`fk_Photo`) REFERENCES `t_photos` (`id_Photo`);

--
-- Contraintes pour la table `t_user_a_equip`
--
ALTER TABLE `t_user_a_equip`
  ADD CONSTRAINT `t_user_a_equip_ibfk_1` FOREIGN KEY (`fk_User`) REFERENCES `t_users` (`id_User`),
  ADD CONSTRAINT `t_user_a_equip_ibfk_2` FOREIGN KEY (`fk_Equipement`) REFERENCES `t_equipement` (`id_Equipement`);

--
-- Contraintes pour la table `t_user_e_prise`
--
ALTER TABLE `t_user_e_prise`
  ADD CONSTRAINT `t_user_e_prise_ibfk_1` FOREIGN KEY (`fk_User`) REFERENCES `t_users` (`id_User`),
  ADD CONSTRAINT `t_user_e_prise_ibfk_2` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
