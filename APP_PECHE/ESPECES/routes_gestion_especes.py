# routes_gestion_especes.py
# AL 2020.05.06 especes des "routes" FLASK pour les especes.
from _md5 import md5

from flask import render_template, flash, redirect, url_for, request, session
from APP_PECHE import app
from APP_PECHE.ESPECES.data_gestion_especes import GestionEspeces
from APP_PECHE.DATABASE.erreurs import *
import os
from werkzeug.utils import secure_filename
# AL 2020.05.10 Pour utiliser les expressions régulières REGEX
import re

# ---------------------------------------------------------------------------------------------------
# AL 2020.05.07 Définition d'une "route" /especes_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/especes_afficher
# ---------------------------------------------------------------------------------------------------
from APP_PECHE.PRISES.routes_gestion_prises import allowed_image


# Pour la tester http://127.0.0.1:1234/especes/afficher
@app.route("/especes/afficher", methods=['GET', 'POST'])
def especes_afficher():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_especes = GestionEspeces()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionespeces()
            # Fichier data_gestion_especes.py
            data_especes = obj_actions_especes.especes_afficher_data()
            # Pour afficher un message dans la console.
            print(" data especes", data_especes, "type ", type(data_especes))

            # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données especes affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # AL 2020.05.12 Envoie la page "HTML" au serveur.
    return render_template("especes/especes_afficher.html", data=data_especes)


# Pour la tester http://127.0.0.1:1234/especes/afficher/admin
@app.route("/especes/afficher/admin", methods=['GET', 'POST'])
def especes_rank_afficher():
    if 'loggedin' in session:
        # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
        # ou un envoi de donnée par des champs du formulaire HTML.
        if request.method == "GET":
            try:
                # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_especes = GestionEspeces()
                # Récupére les données grâce à une requête MySql définie dans la classe Gestionespeces()
                # Fichier data_gestion_especes.py
                data_especes = obj_actions_especes.especes_afficher_byid_data()
                # Pour afficher un message dans la console.
                print(" data especes", data_especes, "type ", type(data_especes))

                # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données especes affichées !!", "Success")
            except Exception as erreur:
                print(f"RGG Erreur générale.")
                # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
                # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
                raise Exception(f"RGG Erreur générale. {erreur}")
                # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

        # AL 2020.05.12 Envoie la page "HTML" au serveur.
        return render_template("especes/especes_rank_afficher.html", data=data_especes)
    return redirect(url_for('login'))


@app.route("/especes/afficher/search", methods=['GET', 'POST'])
def especes_search ():
    # AL 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # AL 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_espece = GestionEspeces()

            # AL 2020.04.09 Récupère le contenu du champ dans le formulaire HTML
            SearchEspece = request.form['search_espece_html']
            
            if re.match("^$",
                            SearchEspece):

                # AL 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
                # On doit afficher à nouveau le formulaire "genres_add.html" à cause des erreurs de "claviotage"
                return render_template("especes/especes_afficher.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_SearchEspece": SearchEspece}
                data_search_espece = obj_actions_espece.research_espece_data(valeurs_insertion_dictionnaire)
                # AL 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'genres_afficher', car l'utilisateur
                # doit voir le nouveau genre qu'il vient d'insérer. Et on l'affiche de manière
                # à voir le dernier élément inséré.
                return render_template("especes/especes_afficher.html",data=data_search_espece)

        # AL 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # AL 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # AL 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # AL 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # AL 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("especes/especes_afficher.html")

# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /profil_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "profil_add.html"
# Pour la tester http://127.0.0.1:1234/profil_add
# ---------------------------------------------------------------------------------------------------
@app.route("/especes/add", methods=['GET', 'POST'])
def especes_add():
    # Anthony L 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_especes = GestionEspeces()
            # Anthony L 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "espece_add.html"
            NomEspece = request.values['NomEspece_html']
            NomScientifiqueEspece = request.values['NomScientifiqueEspece_html']
            DescriptionEspece = request.values['DescriptionEspece_html']
            PoidsMaxEspece = request.values['PoidsMaxEspece_html']
            LongMaxEspece = request.values['LongMaxEspece_html']
            AgeMaxEspece = request.values['AgeMaxEspece_html']
            ProfEspece = request.values['ProfEspece_html']
            PlanEauEspece = request.values['PlanEauEspece_html']
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files["file"]

            # vérification de la présence d'un nom à l'image
            if file.filename == "":
                flash("No filename")
                return redirect(request.url)
            # Anthony L 2019.04.04 On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-z])[a-z]*(-)?[a-z]+$",
                            NomEspece):
                # Anthony L 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Le nom est...incorrecte !!", "Danger")
                # On doit afficher à nouveau le formulaire "profil_add.html" à cause des erreurs de "claviotage"
                return render_template("especes/especes_add.html")

            else:
                # vérification de la validité de l'image
                if file and allowed_image(file.filename):
                    name = secure_filename(file.filename)
                    filename = NomEspece + "." + name.rsplit(".", 1)[1]
                    # sauvegarde de l'image
                    file.save(os.path.join(app.config["THUMBNAIL_UPLOADS"], filename))

                    # Définition du dictionnaire
                    valeurs_insertion_dictionnaire = {"value_NomEspece": NomEspece,
                                                      "value_NomScientifiqueEspece": NomScientifiqueEspece,
                                                      'value_DescriptionEspece': DescriptionEspece,
                                                      'value_PoidsMaxEspece': PoidsMaxEspece,
                                                      'value_LongMaxEspece': LongMaxEspece,
                                                      'value_AgeMaxEspece': AgeMaxEspece,
                                                      'value_ProfEspece': ProfEspece,
                                                      'value_PlanEauEspece': PlanEauEspece,
                                                      'value_ThumbnailEspeceLink': filename}

                    obj_actions_especes.add_espece_data(valeurs_insertion_dictionnaire)

                # Anthony L 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux
                # utilisateurs.
                flash(f"Données insérées !!", "Sucess")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'especes_afficher', car l'utilisateur
                # doit voir le nouveau espece qu'il vient d'insérer.
                return redirect(url_for('especes_rank_afficher'))

        # Anthony L 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Anthony L 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Anthony L 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # Anthony L 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # Anthony L 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Anthony L 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("especes/especes_add.html")


@app.route("/especes/upload", methods=['GET', 'POST'])
def espece_upload_file(valeurs_insertion_dictionnaire):
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if 'loggedin' in session:
        if request.method == "POST":
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files["file"]

            # vérification de la présence d'un nom à l'image
            if file.filename == "":
                flash("No filename")
                return redirect(request.url)
            # vérification de la validité de l'image
            if file and allowed_image(file.filename):
                name = secure_filename(file.filename)
                filename = valeurs_insertion_dictionnaire['value_NomEspece'] + "." + name.rsplit(".", 1)[1]
                # sauvegarde de l'image
                file.save(os.path.join(app.config["IMAGE_UPLOADS"], filename))

                # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_especes = GestionEspeces()
                # Définition du dictionnaire
                valeurs_insertion_dictionnaire['value_ThumbnailEspeceLink'] = filename
                obj_actions_especes.add_espece_data(valeurs_insertion_dictionnaire)

                print("Image saved")
                return redirect(url_for('especes_rank_afficher'))
        return render_template("especes/especes_upload.html")

    return redirect(url_for("login"))


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /especes_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un espece de especes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/especes/edit', methods=['POST', 'GET'])
def especes_edit():
    # Anthony L 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "especes_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_espece" du formulaire html "especes_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_espece"
            # grâce à la variable "id_Espece_edit_html"
            # <a href="{{ url_for('especes_edit', id_Espece_edit_html=row.id_espece) }}">Edit</a>
            id_espece_edit = request.values['id_Espece_edit_html']

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_espece": id_espece_edit}

            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_especes = GestionEspeces()

            # Anthony L 2019.04.02 La commande MySql est envoyée à la BD
            data_id_espece = obj_actions_especes.edit_espece_data(valeur_select_dictionnaire)
            print("dataIdespece ", data_id_espece, "type ", type(data_id_espece))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le espece d'un film !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # Anthony L 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("especes/especes_edit.html", data=data_id_espece)


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /especes_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un espece de especes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/especes/update', methods=['POST', 'GET'])
def especes_update():
    # Anthony L 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "especes_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du espece alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:

            # Récupérer la valeur de "id_espece" du formulaire html "especes_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_espece"
            # grâce à la variable "id_Espece_edit_html"
            # <a href="{{ url_for('especes_edit', id_Espece_edit_html=row.id_espece) }}">Edit</a>
            id_espece_edit = request.values['id_Espece_edit_html']

            # Récupère le contenu du champ "NomPers" dans le formulaire HTML "especesEdit.html"
            NomEspece = request.values['edit_NomEspece_html']
            NomScientifiqueEspece = request.values['edit_NomScientifiqueEspece_html']
            DescriptionEspece = request.values['edit_DescriptionEspece_html']
            PoidsMaxEspece = request.values['edit_PoidsMaxEspece_html']
            LongMaxEspece = request.values['edit_LongMaxEspece_html']
            AgeMaxEspece = request.values['edit_AgeMaxEspece_html']
            ProfEspece = request.values['edit_ProfEspece_html']
            PlanEauEspece = request.values['edit_PlanEauEspece_html']

            valeur_edit_list = [{'id_espece': id_espece_edit, 'NomEspece': NomEspece,
                                 'NomScientifiqueEspece': NomScientifiqueEspece,
                                 'DescriptionEspece': DescriptionEspece,
                                 'PoidsMaxEspece': PoidsMaxEspece,
                                 'LongMaxEspece': LongMaxEspece,
                                 'AgeMaxEspece': AgeMaxEspece,
                                 'ProfEspece': ProfEspece,
                                 'PlanEauEspece': PlanEauEspece}]

            # Regex n'acceptant que les noms valides
            if not re.match("^([A-Z]|[a-z])[a-z]*(-)?[a-z]+$", NomEspece):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                flash(f"Le nom n'est pas valide !! Pas de chiffres, de caractères spéciaux, d'espace"
                      f"Danger")

                # On doit afficher à nouveau le formulaire "especes_edit.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_espece': id_espece_edit, 'NomEspece': NomEspece,
                                     'NomScientifiqueEspece': NomScientifiqueEspece,
                                     'DescriptionEspece': DescriptionEspece,
                                     'PoidsMaxEspece': PoidsMaxEspece,
                                     'LongMaxEspece': LongMaxEspece,
                                     'AgeMaxEspece': AgeMaxEspece,
                                     'ProfEspece': ProfEspece,
                                     'PlanEauEspece': PlanEauEspece}]

                # Pour afficher le contenu et le type de valeurs passées au formulaire "especes_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('especes/especes_edit.html', data=valeur_edit_list)

            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_espece": id_espece_edit,
                                              "value_NomEspece": NomEspece,
                                              "value_NomScientifiqueEspece": NomScientifiqueEspece,
                                              'value_DescriptionEspece': DescriptionEspece,
                                              'value_PoidsMaxEspece': PoidsMaxEspece,
                                              'value_LongMaxEspece': LongMaxEspece,
                                              'value_AgeMaxEspece': AgeMaxEspece,
                                              'value_ProfEspece': ProfEspece,
                                              'value_PlanEauEspece': PlanEauEspece}

                # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_especes = GestionEspeces()

                # La commande MySql est envoyée à la BD
                data_id_espece = obj_actions_especes.update_espece_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdespece ", data_id_espece, "type ", type(data_id_espece))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer le espece d'un film !!!")
                # On affiche les especes
                return redirect(url_for('especes_rank_afficher'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème especes update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_intitule_espece_html" alors on renvoie le formulaire "EDIT"
            return render_template('especes/especes_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /especes_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un espece de especes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/especes/select_delete', methods=['POST', 'GET'])
def especes_select_delete():
    if request.method == 'GET':
        try:

            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_especes = GestionEspeces()
            # Anthony L 2019.04.04 Récupérer la valeur de "idespeceDeleteHTML" du formulaire html "especesDelete.html"
            id_espece_delete = request.args.get('id_Espece_delete_html')
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_espece": id_espece_delete}

            # Anthony L 2019.04.02 La commande MySql est envoyée à la BD
            data_id_espece = obj_actions_especes.delete_select_espece_data(valeur_delete_dictionnaire)
            flash(f"EFFACER la ligne !")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # Pour afficher un message dans la console.
            print(f"Erreur especes_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur especes_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('especes/especes_delete.html', data=data_id_espece)


# ---------------------------------------------------------------------------------------------------
# Anthony L 2019.04.02 Définition d'une "route" /especesUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un espece, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/especes/delete', methods=['POST', 'GET'])
def especes_delete():
    # Anthony L 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_especes = GestionEspeces()
            # Anthony L 2019.04.02 Récupérer la valeur de "id_espece" du formulaire html "especesAfficher.html"
            id_espece_delete = request.form['id_Espece_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_espece": id_espece_delete}

            data_especes = obj_actions_especes.delete_espece_data(valeur_delete_dictionnaire)
            # Anthony L 2019.04.02 On va afficher la liste des especes des especes
            # Anthony L 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les especes
            return redirect(url_for('especes_rank_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Anthony L 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "espece" de especes qui est associé dans "t_pers_a_especes".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des prises !')
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !! Ce espece est associé à des prises !!! : {erreur}")
                # Afficher la liste des especes des especes
                return redirect(url_for('especes_rank_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # Pour afficher un message dans la console.
                print(f"Erreur especes_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur especes_delete {erreur.args[0], erreur.args[1]}")

            # Anthony L 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('especes/especes_rank_afficher.html', data=data_especes)
